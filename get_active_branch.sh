#!/bin/bash
# gets active git branch
echo "$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')"
