exports.buildTagPush = function (dockerRepository, projectName) {

    var PythonShell = require('python-shell');

    var options = {
        args: ['-r ' + dockerRepository, '-n ' + projectName]
    };

    var scriptPath = './node_modules/docker-manager-nodejs/build-tag-push.py';

    console.log("SCRIPT PATH + " + scriptPath);


    var pyshell = new PythonShell(scriptPath, options);

    // sends a message to the Python script via stdin
    pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        console.log(message);
    });

    // end the input stream and allow the process to exit
    pyshell.end(function (err) {
        if (err) throw err;
        console.log('finished');
    });

}