from distutils.version import LooseVersion
import json
import subprocess
import sys, getopt
import os

# Store input and output file names
registry=''
project_name=''

# Read command line args
myopts, args = getopt.getopt(sys.argv[1:],"r:n:")

###############################
# o == option
# a == argument passed to the o
###############################
for o, a in myopts:
    if o == '-r':
        registry=a.strip()
    elif o == '-n':
        project_name=a.strip()
    else:
        print("Usage: %s -r docker_repository -n project_name" % sys.argv[0])
        exit(1)

# Display input and output file name passed as the args
print ("Docker registry : %s and project name: %s" % (registry,project_name) )



#registry = os.environ.get("DOCKER_REGISTRY")

if not registry:
    print("Please set the DOCKER_REGISTRY variable, e.g.:")
    print("export DOCKER_REGISTRY=jpetazzo # use the Docker Hub")
    print("export DOCKER_REGISTRY=localhost:5000 # use a local registry")
    exit(1)


# get current branch name, taking into consideration possibility of a detached head
sedCommand = 's,.*/\(.*\),\1,'
branch_name = subprocess.check_output("./node_modules/docker-manager-nodejs/get_active_branch.sh",  shell=True).rstrip()

print("branch name : {}".format(branch_name))

# Generate a Docker image tag, nomenclature as decided

#project_name = os.environ.get("PROJECT_NAME")
container_name = "{}/{}".format('local', project_name)

# Execute "docker build", tagging it with a name so we can reference it later,  and abort if it fails.
subprocess.check_call(["docker", "build", "-t", container_name, "."])

# Tag image 2, once with git version and again with label,
# and then push these to a docker repository (may be private or public).

print('INFO: container_name to push: ' + container_name);

# Re-tag the image so that it can be uploaded to the registry.

docker_version = subprocess.check_output(['docker', 'version']).decode().split("\n")[1].split()[1]

must_force_tag = LooseVersion(docker_version) < LooseVersion('1.10')

# nomenclature for docker images will take the following format:
# feature/???? -> image_name:feature_????
# hotfix/???? -> image_name:x.y.z-SNAPSHOT-abbrev
# develop -> image_name:x.y.z-SNAPSHOT-abbrev
# release/???? -> image_name:x.y.0
# 1.0.x format (hotfix source branch) -> image_name:x.y.z

# get app version
with open ("package.json", "r") as myfile:
    data=myfile.read().replace('\n', '')
jsonObj = json.loads(data)
version = jsonObj["version"]

# features and hotfixes images will be called as the branch name but formatted
image_name = branch_name.replace("/", "_")

# release and hotfixes source branches images will be called as the version
if branch_name.startswith( 'release/' ) or branch_name.endswith( '.x' ):
    image_name = version
else:
    # develop and hotfixes images will be called as snapshots of the version
    if branch_name == 'develop' or branch_name.startswith( 'hotfix/' ):
        git_commit_hash = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).rstrip()
        image_name = "{}-{}-{}".format(version, "SNAPSHOT", git_commit_hash)

# registry of the image name is the branch name formatted by default
registry_image_label = "{}/{}:{}".format(registry, project_name, image_name)

# only use force tag if using docker version 1.9 or earlier
if must_force_tag:
    subprocess.check_call(["docker", "tag", "-f", container_name, registry_image_label])
else:
    subprocess.check_call(["docker", "tag", container_name, registry_image_label])
print('INFO: pushing to : ' + registry_image_label);
subprocess.check_call(["docker", "push", registry_image_label])
